﻿using Amazon;
using Amazon.S3;
using Core;
using Core.AmazonS3;
using Core.FileSystem;
using Core.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Consumer
{
    internal class Program
    {
        private static bool _exit;

        private static void Main()
        {
            Console.WriteLine("Press CTRL+C to exit");
            Console.WriteLine($"[Consumer] Local File Storage: {LocalFileSystem.GetTempPath()}");
            Console.CancelKeyPress += (sender, args) =>
            {
                Console.WriteLine("  >> exiting...");
                args.Cancel = _exit = true;
            };

            //golden record copy
            var items = new List<Person>();

            //consumer
            var consumer = UseAmazonS3Consumer<Person>();
            consumer.Subscribe(() =>
            {
                Console.Write("[Consumer] Dataset read with ");

                items.Clear();
                consumer.Read(item => items.Add(item));

                Console.WriteLine($"{items.Count:N0} items.");
            });

            //loop
            while (!_exit)
            {
                Task.Delay(1000).Wait();
            }

            //exit
            consumer.Dispose();
        }

        private static Consumer<T> UseFileSystemConsumer<T>()
        {
            var path = Path.Combine(LocalFileSystem.GetTempPath(), "person");
            Console.WriteLine($"[Consumer] File System: {path}");
            return new FileSystemConsumer<T>(path);
        }

        private static Consumer<T> UseAmazonS3Consumer<T>()
        {
            const string bucket = "b932e8d3bf654c69b114f94bb5b2d436";
            const string prefix = "datasets/person";
            Console.WriteLine($"[Consumer] S3 Bucket: {bucket} Prefix: {prefix}");
            var amazonS3 = new AmazonS3Client(new AmazonS3Config { RegionEndpoint = RegionEndpoint.USEast1 });
            return new AmazonS3Consumer<T>(bucket, prefix, amazonS3);
        }

        public class Person
        {
            public Guid Id { get; set; }
            public DateTimeOffset Timestamp { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Username { get; set; }
            public string Email { get; set; }
        }
    }
}
