﻿using Amazon.S3;
using System;
using System.IO;
using System.Threading;
using System.Timers;
using Core.Util;
using Timer = System.Timers.Timer;

namespace Core.AmazonS3
{
    public class AmazonS3Consumer<T> : Consumer<T>
    {
        private readonly string _bucket;
        private readonly string _prefix;
        private readonly AmazonS3Client _amazonS3;
        private readonly Timer _timer;
        private bool _disposed;
        private string _datasetId;
        private string _path;

        public AmazonS3Consumer(string bucket, string prefix, AmazonS3Client amazonS3)
        {
            _bucket = bucket ?? throw new ArgumentNullException(nameof(bucket));
            _prefix = prefix?.TrimEnd('/') ?? throw new ArgumentNullException(nameof(prefix));
            _amazonS3 = amazonS3 ?? throw new ArgumentNullException(nameof(amazonS3));

            _timer = new Timer { Interval = 1000, AutoReset = false };
            _timer.Elapsed += OnTimerElapsed;
            _timer.Start();
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs args)
        {
            try
            {
                var datasetId = GetAnnouncedDatasetId();
                if (datasetId == _datasetId)
                    return;

                var path = DownloadDataset(datasetId);
                OnDatasetPathChange(path);
                DeleteDataset(_path);

                _path = path;
                _datasetId = datasetId;
            }
            finally
            {
                if (!_disposed)
                    _timer?.Start();
            }
        }

        private string GetAnnouncedDatasetId()
        {
            try
            {
                var response = _amazonS3.GetObjectMetadataAsync(_bucket, $"{_prefix}/announced.version").ConfigureAwait(false).GetAwaiter().GetResult();
                return response.Metadata["dataset"];
            }
            catch
            {
                return null;
            }
        }

        private string DownloadDataset(string id)
        {
            var path = LocalFileSystem.CreateTempFile();
            var response = _amazonS3.GetObjectAsync(_bucket, $"{_prefix}/{id}").ConfigureAwait(false).GetAwaiter().GetResult();
            response.WriteResponseStreamToFileAsync(path, false, CancellationToken.None).ConfigureAwait(false).GetAwaiter().GetResult();
            return path;
        }

        private static void DeleteDataset(string path)
        {
            try
            {
                File.Delete(path);
            }
            catch
            {
                //ignored
            }
        }

        public override void Dispose()
        {
            _disposed = true;
            _timer?.Dispose();
            _amazonS3?.Dispose();
            DeleteDataset(_path);
        }
    }
}
