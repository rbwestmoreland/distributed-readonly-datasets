﻿using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Core.AmazonS3
{
    public class AmazonS3Producer<T> : Producer<T>
    {
        private readonly string _bucket;
        private readonly string _prefix;
        private readonly AmazonS3Client _amazonS3;

        public AmazonS3Producer(string bucket, string prefix, AmazonS3Client amazonS3)
        {
            _bucket = bucket ?? throw new ArgumentNullException(nameof(bucket));
            _prefix = prefix?.TrimEnd('/') ?? throw new ArgumentNullException(nameof(prefix));
            _amazonS3 = amazonS3 ?? throw new ArgumentNullException(nameof(amazonS3));
        }

        protected override Task PublishDatasetAsync(string path, string id, CancellationToken cancellationToken)
        {
            return _amazonS3.PutObjectAsync(new PutObjectRequest
            {
                BucketName = _bucket,
                Key = $"{_prefix}/{id}",
                FilePath = path,
                StorageClass = S3StorageClass.Standard
            }, cancellationToken);
        }

        protected override Task AnnounceDatasetAsync(string id, CancellationToken cancellationToken)
        {
            return _amazonS3.PutObjectAsync(new PutObjectRequest
            {
                BucketName = _bucket,
                Key = $"{_prefix}/announced.version",
                ContentBody = id,
                StorageClass = S3StorageClass.Standard,
                Metadata = { ["dataset"] = id }
            }, cancellationToken);
        }

        protected override Task CleanupDatasetsAsync(CancellationToken cancellationToken)
        {
            //todo
            return Task.CompletedTask;
        }

        public override void Dispose()
        {
            _amazonS3?.Dispose();
        }
    }
}
