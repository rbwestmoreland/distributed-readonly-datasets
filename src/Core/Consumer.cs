﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace Core
{
    public abstract class Consumer<T> : IDisposable
    {
        private EventHandler _changed;
        private string _datasetPath;
        private readonly object _datasetLock = new object();

        public void Read(Action<T> callback)
        {
            lock (_datasetLock)
            {
                using (var fileStream = File.Open(_datasetPath, FileMode.Open, FileAccess.Read, FileShare.Read))
                using (var gzipStream = new GZipStream(fileStream, CompressionMode.Decompress))
                using (var streamReader = new StreamReader(gzipStream, Encoding.UTF8))
                using (var jsonReader = new JsonTextReader(streamReader) { SupportMultipleContent = true })
                {
                    var serializer = new JsonSerializer();
                    while (jsonReader.Read())
                    {
                        var item = serializer.Deserialize<T>(jsonReader);
                        callback(item);
                    }
                }
            }
        }

        public void Subscribe(Action callback)
        {
            _changed += (sender, args) => callback();

            if (_datasetPath != null)
                callback();
        }

        protected void OnDatasetPathChange(string path)
        {
            if (_datasetPath == path)
                return;

            lock (_datasetLock)
            {
                if (_datasetPath == path)
                    return;

                _datasetPath = path;
                _changed?.Invoke(this, EventArgs.Empty);
            }
        }

        public abstract void Dispose();
    }
}
