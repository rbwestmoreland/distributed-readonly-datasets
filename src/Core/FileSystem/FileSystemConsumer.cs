﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Core.FileSystem
{
    public class FileSystemConsumer<T> : Consumer<T>
    {
        private readonly string _path;
        private readonly FileSystemWatcher _fileSystemWatcher;

        public FileSystemConsumer(string path)
        {
            _path = path ?? throw new ArgumentNullException(nameof(path));
            _fileSystemWatcher = new FileSystemWatcher(_path)
            {
                EnableRaisingEvents = true
            };

            _fileSystemWatcher.Changed += OnChanged;
            OnChanged(this, new FileSystemEventArgs(WatcherChangeTypes.Changed, _path, "announced.version"));
        }

        private void OnChanged(object sender, FileSystemEventArgs args)
        {
            try
            {
                if (args.Name != "announced.version")
                    return;

                if (!File.Exists(args.FullPath))
                    return;

                Task.Delay(500).Wait();

                var id = File.ReadAllText(args.FullPath);
                var datasetPath = Path.Combine(_path, id);

                OnDatasetPathChange(datasetPath);
            }
            catch (IOException)
            {
                OnChanged(sender, args);
            }
        }

        public override void Dispose()
        {
            _fileSystemWatcher?.Dispose();
        }
    }
}