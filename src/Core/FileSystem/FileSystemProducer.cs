﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Core.FileSystem
{
    public class FileSystemProducer<T> : Producer<T>
    {
        private readonly string _path;

        public FileSystemProducer(string path)
        {
            _path = path ?? throw new ArgumentNullException(nameof(path));
        }

        protected override Task PublishDatasetAsync(string path, string id, CancellationToken cancellationToken)
        {
            var destination = Path.Combine(_path, id);
            Directory.CreateDirectory(_path);
            File.Move(path, destination);

            return Task.CompletedTask;
        }

        protected override async Task AnnounceDatasetAsync(string id, CancellationToken cancellationToken)
        {
            var destination = Path.Combine(_path, "announced.version");

            Directory.CreateDirectory(_path);

            using (var fileStream = File.Open(destination, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
            using (var streamWriter = new StreamWriter(fileStream, Encoding.UTF8))
            {
                fileStream.SetLength(0);
                await streamWriter.WriteAsync(id).ConfigureAwait(false);
            }
        }

        protected override Task CleanupDatasetsAsync(CancellationToken cancellationToken)
        {
            foreach (var path in Directory.EnumerateFiles(_path))
            {
                try
                {
                    var fileName = Path.GetFileName(path);
                    if (fileName == "announced.version")
                        continue;

                    var lastAccess = File.GetLastAccessTimeUtc(path);
                    var age = DateTime.UtcNow - lastAccess;
                    if (age > TimeSpan.FromMinutes(1))
                        File.Delete(path);
                }
                catch
                {
                    // ignored
                }
            }

            return Task.CompletedTask;
        }

        public override void Dispose()
        {
        }
    }
}
