﻿using Core.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Core
{
    public abstract class Producer<T> : IDisposable
    {
        public async Task CreateDatasetAsync(IEnumerable<T> items, CancellationToken cancellationToken)
        {
            var datasetId = $"{DateTimeOffset.UtcNow.ToUnixTimeSeconds()}.dataset";
            var datasetPath = LocalFileSystem.CreateTempFile();

            //create
            CreateDataset(datasetPath, items);

            //publish
            await PublishDatasetAsync(datasetPath, datasetId, cancellationToken).ConfigureAwait(false);

            //announce
            await AnnounceDatasetAsync(datasetId, cancellationToken).ConfigureAwait(false);

            //clean up
            await CleanupDatasetsAsync(cancellationToken).ConfigureAwait(false);

            try
            {
                File.Delete(datasetPath);
            }
            catch
            {
                // ignored
            }
        }

        protected abstract Task PublishDatasetAsync(string path, string id, CancellationToken cancellationToken);
        protected abstract Task AnnounceDatasetAsync(string id, CancellationToken cancellationToken);
        protected abstract Task CleanupDatasetsAsync(CancellationToken cancellationToken);

        public abstract void Dispose();

        private static void CreateDataset(string path, IEnumerable<T> items)
        {
            using (var fileStream = File.Open(path, FileMode.Open, FileAccess.Write, FileShare.None))
            using (var gzipStream = new GZipStream(fileStream, CompressionMode.Compress))
            using (var streamWriter = new StreamWriter(gzipStream, Encoding.UTF8))
            using (var jsonWriter = new JsonTextWriter(streamWriter))
            {
                var serializer = new JsonSerializer { Formatting = Formatting.None };
                foreach (var item in items)
                {
                    serializer.Serialize(jsonWriter, item);
                    jsonWriter.WriteWhitespace(Environment.NewLine);
                }
            }
        }
    }
}
