﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Core.Util
{
    public class Benchmark : IDisposable
    {
        private readonly string _description;
        private readonly Stopwatch _stopwatch;

        public Benchmark(string description)
        {
            _description = description;
            _stopwatch = Stopwatch.StartNew();
        }

        public void Dispose()
        {
            Console.WriteLine($"{_description} took {_stopwatch.Elapsed.TotalSeconds:F2}s");
        }

        public static void Run(string description, Action action)
        {
            using (new Benchmark(description))
                action();
        }

        public static T Run<T>(string description, Func<T> action)
        {
            using (new Benchmark(description))
                return action();
        }

        public static async Task RunAsync(string description, Func<Task> action)
        {
            using (new Benchmark(description))
                await action().ConfigureAwait(false);
        }

        public static async Task<T> RunAsync<T>(string description, Func<Task<T>> action)
        {
            using (new Benchmark(description))
                return await action().ConfigureAwait(false);
        }
    }
}
