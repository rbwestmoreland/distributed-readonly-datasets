﻿using System.IO;

namespace Core.Util
{
    public static class LocalFileSystem
    {
        public static string GetTempPath() => Path.Combine(Path.GetTempPath(), "datasets");

        public static string CreateTempFile()
        {
            var temp = Path.GetTempFileName();
            var path = Path.Combine(GetTempPath(), "temp", Path.GetFileName(temp));
            Directory.CreateDirectory(Path.GetDirectoryName(path));
            File.Move(temp, path);
            return path;
        }
    }
}
