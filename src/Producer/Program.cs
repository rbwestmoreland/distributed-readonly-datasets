﻿using Amazon;
using Amazon.S3;
using Core.AmazonS3;
using Core.FileSystem;
using Core.Util;
using Producer.Repository;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Producer
{
    internal class Program
    {
        private static bool _exit;
        private static PersonRepository _repository;

        private static void Main()
        {
            Console.WriteLine("Press CTRL+C to exit");
            Console.WriteLine($"[Producer] Local File Storage: {LocalFileSystem.GetTempPath()}");
            Console.CancelKeyPress += (sender, args) =>
            {
                Console.WriteLine("  >> exiting...");
                args.Cancel = _exit = true;
            };

            //golden record
            _repository = new PersonRepository(100000);

            //producer
            var producer = UseAmazonS3Producer<Person>();

            //loop
            while (!_exit)
            {
                var items = _repository.GetAllAsync(CancellationToken.None).GetAwaiter().GetResult().ToArray();
                producer.CreateDatasetAsync(items, CancellationToken.None).GetAwaiter().GetResult();

                Console.WriteLine($"[Producer] Dataset created with {items.Length:N0} items.");

                _repository.AddAsync(1, CancellationToken.None).GetAwaiter().GetResult();
                Task.Delay(10000).Wait();
            }

            //exit
            producer.Dispose();
        }

        private static Core.Producer<T> UseFileSystemProducer<T>()
        {
            var path = Path.Combine(LocalFileSystem.GetTempPath(), "person");
            Console.WriteLine($"[Producer] File System: {path}");
            return new FileSystemProducer<T>(path);
        }

        private static Core.Producer<T> UseAmazonS3Producer<T>()
        {
            const string bucket = "b932e8d3bf654c69b114f94bb5b2d436";
            const string prefix = "datasets/person";
            Console.WriteLine($"[Producer] S3 Bucket: {bucket} Path: {prefix}");
            var amazonS3 = new AmazonS3Client(new AmazonS3Config { RegionEndpoint = RegionEndpoint.USEast1 });
            return new AmazonS3Producer<T>(bucket, prefix, amazonS3);
        }
    }
}
