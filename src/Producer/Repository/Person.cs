﻿using System;

namespace Producer.Repository
{
    public class Person
    {
        public Guid Id { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
    }
}
