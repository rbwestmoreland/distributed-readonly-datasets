﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Bogus;

namespace Producer.Repository
{
    public class PersonRepository
    {
        private readonly Faker<Person> _faker;
        private readonly List<Person> _store;

        public PersonRepository(int count)
        {
            _faker = new Faker<Person>()
                .RuleFor(u => u.Id, f => Guid.NewGuid())
                .RuleFor(u => u.Timestamp, f => DateTimeOffset.UtcNow)
                .RuleFor(u => u.FirstName, (f, u) => f.Name.FirstName(f.PickRandom<Bogus.DataSets.Name.Gender>()))
                .RuleFor(u => u.LastName, (f, u) => f.Name.LastName(f.PickRandom<Bogus.DataSets.Name.Gender>()))
                .RuleFor(u => u.Username, (f, u) => f.Internet.UserName(u.FirstName, u.LastName))
                .RuleFor(u => u.Email, (f, u) => f.Internet.Email(u.FirstName, u.LastName));

            _store = _faker.Generate(count);
        }

        public Task AddAsync(int count, CancellationToken cancellationToken)
        {
            _store.AddRange(_faker.Generate(count));
            return Task.CompletedTask;
        }

        public Task<IEnumerable<Person>> GetAllAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult<IEnumerable<Person>>(_store.ToArray());
        }
    }
}
